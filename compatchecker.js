//
// Compatability checker for SecMSG.
//
// Note: Update this every time a newer function is used!
//
const funclist = [

];

eventManager.on("pageload", () => {
	_.forEach(funclist, (element) => {
		if (typeof element.func === "undefined") {
			document.getElementById("checkererrors").innerHTML += "Error: function " +
				element.name + " doesn't exist!";
		} else if (typeof element.func === "function") {
			document.getElementById("checkerstatus").innerHTML = "Found function " +
				element.name;
		} else {
			document.getElementById("checkererrors").innerHTML += "Warn : function " +
				element.name + " exists, but is not a function?";
		}
	});
	if (_.isEmpty(document.getElementById("checkererrors").innerHTML)) {
		eventManager.emit("compatchecker-success");
	} else {
		document.getElementById("checkerstatus").innerHTML = "Errors occurred, upgrade your browser.";
	}
});

eventManager.on("compatchecker-success", () => {
	document.getElementById("checker").style.display = "none";
	document.getElementById("content").style.display = "block";
});
