//
// SecMSG main file
//
// Initialize the EventEmitter
const eventManager = new EventEmitter();
window.eventManager = eventManager;

// Load in the other files
// HACK To make xo happy, we assign them to nop, and then get the name property.
let nop = require("./compatchecker");

nop = window.noop3;
nop();

// Basically $(document).ready
document.addEventListener("DOMContentLoaded", () => {
	eventManager.emit("pageload");
});
