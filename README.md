# SecMSG ![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg) ![Uses Webpack](https://img.shields.io/badge/bundler-webpack-blue.svg)
**Sec** ure **M** es **s** agin **g**  
A very simple secure messaging thing that uses ~~excellent~~
terrible encryption techniques.
## Encryption
Let's say we want to encrypt... `Hello, World!`. First, convert
it to ASCII hex:
```
H  e  l  l  o  ,     W  o  r  l  d  !
48 65 6C 6C 6F 2C 20 57 6F 72 6C 64 21
```
Now, get the charcodes of all of that gunk. I wrote a small
JavaScript script to do this:
```js
var string = "48 65 6C 6C 6F 2C 20 57 6F 72 6C 64 21"
for(var i = 0; i < string.length; i++) {
	if(string.charCodeAt(i) !== 32 /* space */) {
		console.log(string.charCodeAt(i))
	}
}
```
```
52 56 54 53 54 67 54 67 54 70 50 67 50 48 53 55 54 70 55 50 54 67 54 52 50 49
```
With that, we can finally get to encryption.
### Actually Encrypting
Now, we can use simple arithmetic to encrypt our data. Say we
have a random number generator to decide what our arithmetic
would be.
```js
// OPERATORS:
//   0 = +
//   1 = *
// Don't subtract or divide because of negative numbers or large decimals.
//
// Uses lodash/Underscore's _.random method.
let operator = _.random(0, 2);
let arg = _.random(0, 2048);

console.log(operator === 0 ? "+" : "*", arg);
```
Let's do an example run:
```
$ node decide.js
+ 563
```
Okay, we need to add 563 to get to our encrypted message. Slight
modification to the hex-to-charcode script can do this:
```diff
 for(var i = 0; i < string.length; i++) {
     if(string.charCodeAt(i) !== 32 /* space */) {
-        console.log(string.charCodeAt(i))
+        console.log(string.charCodeAt(i) + 563)
     }
 }
```
Now, we get:
```
615 619 617 616 617 630 617 630 617 633 613 630 613 611 616 618 617 633 618 613 617 630 617 615 613 612
```
Yay! Our message is encrypted! Time to decrypt.

## Decryption
To decrypt, we first need to subtract *x*, 563, from our result.
It's easiest to show in code, so here it is.
```js
var nums = "615 619 617 616 617 630 617 630 617 633 613 630 613 611 616 618 617 633 618 613 617 630 617 615 613 612";
for(var i = 0; i < nums.split(" ").length; i++) {
	console.log(Number(nums.split(" ")[i]) - 563);
}
```
```
52 56 54 53 54 67 54 67 54 70 50 67 50 48 53 55 54 70 55 50 54 67 54 52 50 49
```
Seems familiar?

### Parsing that Meaningless String of Numbers
We need to convert that to hexadecimal, so then we can parse the hex. So, let's
do that with some more code.
> Side note: Probably should be writing more spec, not code. But hey, it gets
> the point across.

```js
var nums = "52 56 54 53 54 67 54 67 54 70 50 67 50 48 53 55 54 70 55 50 54 67 54 52 50 49";
for(var i = 0; i < nums.split(" ").length; i++) {
	console.log(String.fromCharCode(Number(nums.split(" ")[i])));
}
```
```
NOTE: Pretty-printed.
48 65 6C 6C 6F 2C 20 57 6F 72 6C 64 21
```
Now, originally I used [hexed.it](https://hexed.it) to make the hexadecimal, but
now I need to do it in JavaScript. So, this will work.
```js
// NOTE Thank you to "Delan Azabani" on SO for this code! :D
function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}
// NOTE Okay, it's mine from here.
var string = "48656C6C6F2C20576F726C6421"

console.log(hex2a(string));
```
```
Hello, World!
```
:tada: :+1:
## Improvements
* **Change the operation every second character.** For example, 1 2 1 with +3,\*2 is
444.
* **Allow multiple operations per.** For example, 1 2 1 with (+1,\*2),(\*2,+1) is
454.
## Linting, Testing and Packing
```shell
# Linting (xo)
npm run lint
# Testing (TODO)
#
# Packing (webpack)
npm run build
```
