// This is a hack!
const path = require("path");

const _ = {
	forEach: require("lodash.foreach"),
	find: require("lodash.find")
};

module.exports = require("lodash.merge")(require("./package.json").webpack, {
	// Contents of hack
	output: {
		path: path.join(__dirname, "/dist")
	}
});

let parent = module.exports;

const forEachFunction = (element, key) => {
	if (element === Object(element)) {
		const _parent = parent;
		parent = element;
		_.forEach(element, forEachFunction);
		const __parent = parent;
		parent = _parent;
		parent[key] = __parent;
	} else if (element.toString().startsWith("/") && element.toString().endsWith("/")) {
		let toRegexify = element.toString().substring(1, element.toString().length);
		toRegexify = toRegexify.substring(0, toRegexify.length - 1);
		parent[key] = new RegExp(toRegexify, "");
	}
};
_.forEach(module.exports, forEachFunction);
