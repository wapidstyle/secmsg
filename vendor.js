/* eslint no-unused-vars: "off" */
//
// Import declarations for vendors.
//
import _ from "lodash";
import {default as EventEmitter} from "eventemitter3";
import {default as noop3} from "noop3";

window._ = _;
window.EventEmitter = EventEmitter;
window.noop3 = noop3;
